package com.slg.immo.features.listings.domain

import com.slg.immo.features.listings.domain.repository.RealEstateAdRepository
import com.slg.immo.utils.REAL_ESTATE_AD_DETAILS
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.mock

class GetRealEstateAdByIdUseCaseTest {
    private val mockRealEstateAdRepository: RealEstateAdRepository = mock()

    private val sut = GetRealEstateAdByIdUseCase(mockRealEstateAdRepository)

    @Test
    fun `should retrieve real estate ad details by id`() = runBlocking {
        //init
        Mockito.`when`(mockRealEstateAdRepository.fetchRealEstateAdById(1))
            .thenReturn(flow { emit(REAL_ESTATE_AD_DETAILS) })

        //run
        val first = sut(1).first()

        //verify
        Assert.assertEquals(REAL_ESTATE_AD_DETAILS, first)
    }
}