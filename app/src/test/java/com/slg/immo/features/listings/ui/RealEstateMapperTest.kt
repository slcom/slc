package com.slg.immo.features.listings.ui

import android.content.Context
import android.content.res.Resources
import com.slg.immo.R
import com.slg.immo.common.utils.formatPrice
import com.slg.immo.utils.REAL_ESTATE_AD_DETAILS
import com.slg.immo.utils.REAL_ESTATE_AD_DETAILS_UI_MODEL
import org.junit.Assert
import org.junit.Test
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.mock

class RealEstateMapperTest {


    private val resources = mock<Resources> {
        on { getQuantityString(R.plurals.bedroom_label, 7, 7) } doAnswer {
            "7 chambres"
        }
        on { getQuantityString(R.plurals.room_label, 11, 11) } doAnswer {
            "11 pièces"
        }
    }
    private val mockContext: Context = mock() {
        on { resources } doAnswer {
            resources
        }
    }

    private val sut = RealEstateMapper(mockContext)

    @Test
    fun `should map details data with plurals`() {
        val price = REAL_ESTATE_AD_DETAILS.price.formatPrice()
        Assert.assertEquals(
            REAL_ESTATE_AD_DETAILS_UI_MODEL.copy(price = price),
            sut.toUiModel(REAL_ESTATE_AD_DETAILS)
        )
    }
}