package com.slg.immo.features.listings.ui.list

import com.slg.immo.features.listings.domain.GetRealEstateListingsUseCase
import com.slg.immo.features.listings.domain.model.RealEstateListings
import com.slg.immo.features.listings.ui.RealEstateMapper
import com.slg.immo.features.listings.ui.list.model.OldDataWarning
import com.slg.immo.features.listings.ui.list.model.RealEstateListingState
import com.slg.immo.utils.CoroutinesTest
import com.slg.immo.utils.REAL_ESTATE_LISTING
import com.slg.immo.utils.REAL_ESTATE_LISTING_UI_MODEL
import com.slg.immo.utils.collectEffect
import com.slg.immo.utils.collectStates
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.mock

class RealEstateListingsViewModelTest : CoroutinesTest() {

    private val mockMapper: RealEstateMapper = mock()
    private val mockGetRealEstateListings: GetRealEstateListingsUseCase = mock()

    private val sut =
        RealEstateListingsViewModel(testDispatcher, mockGetRealEstateListings, mockMapper)


    @Test
    fun `should fetch real estate listing`() = runBlocking {
        //init
        Mockito.`when`(mockMapper.toUiModel(REAL_ESTATE_LISTING)).thenReturn(
            REAL_ESTATE_LISTING_UI_MODEL
        )
        Mockito.`when`(mockGetRealEstateListings())
            .thenReturn(flow { emit(RealEstateListings(REAL_ESTATE_LISTING, true)) })
        val states = sut.collectStates()

        //run
        sut.start()

        //verify
        assertEquals(
            listOf(
                RealEstateListingState.Loading,
                RealEstateListingState.Result(REAL_ESTATE_LISTING_UI_MODEL)
            ),
            states
        )
    }

    @Test
    fun `should return empty result if there is no real estate ad`() = runBlocking {
        //init
        Mockito.`when`(mockMapper.toUiModel(emptyList())).thenReturn(emptyList())
        Mockito.`when`(mockGetRealEstateListings())
            .thenReturn(flow { emit(RealEstateListings(emptyList(), false)) })
        val states = sut.collectStates()

        //run
        sut.start()

        //verify
        assertEquals(
            listOf(RealEstateListingState.Loading, RealEstateListingState.EmptyResult),
            states
        )
    }

    @Test
    fun `should display warning if data come from local storage`() = runBlocking {
        //init
        Mockito.`when`(mockMapper.toUiModel(REAL_ESTATE_LISTING)).thenReturn(
            REAL_ESTATE_LISTING_UI_MODEL
        )
        Mockito.`when`(mockGetRealEstateListings())
            .thenReturn(flow { emit(RealEstateListings(REAL_ESTATE_LISTING, false)) })
        val effects = sut.collectEffect()

        //run
        sut.start()

        //verify
        assertEquals(listOf(OldDataWarning), effects)
    }

    @Test
    fun `should display empty result on failure`() = runBlocking {
        //init
        Mockito.`when`(mockGetRealEstateListings())
            .thenReturn(flow { throw IllegalStateException() })
        val states = sut.collectStates()
        //run
        sut.start()

        //verify
        assertEquals(
            listOf(RealEstateListingState.Loading, RealEstateListingState.EmptyResult),
            states
        )
    }
}