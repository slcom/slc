package com.slg.immo.features.listings.data

import com.slg.immo.features.listings.data.local.RealEstateAdLocalDataSource
import com.slg.immo.features.listings.data.model.RealEstateAdRaw
import com.slg.immo.features.listings.data.remote.RealEstateAdDataSource
import com.slg.immo.features.listings.domain.model.RealEstateAd
import com.slg.immo.features.listings.domain.model.RealEstateAdDetails
import com.slg.immo.features.listings.domain.model.RealEstateListings
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.inOrder
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class ImmoRealEstateAdRepositoryTest {
    private val mockRealEstateAdDataSource: RealEstateAdDataSource = mock()

    private val mockRealEstateAdLocalDataSource: RealEstateAdLocalDataSource = mock()

    private lateinit var sut: ImmoRealEstateAdRepository

    @Before
    fun setUp() {
        sut =
            ImmoRealEstateAdRepository(mockRealEstateAdDataSource, mockRealEstateAdLocalDataSource)
    }

    @Test
    fun `should retrieve all real estate listings and store them in local`() = runBlocking {
        //init
        Mockito.`when`(mockRealEstateAdDataSource.fetchAds()).thenReturn(REAL_ESTATE_LISTING_RAW)
        //run
        val first = sut.fetchRealEstateListings().first()

        //verify
        verify(mockRealEstateAdLocalDataSource).insertAll(REAL_ESTATE_LISTING_RAW)
        Assert.assertEquals(RealEstateListings(REAL_ESTATE_AD, true), first)
    }

    @Test
    fun `should return local real estate listings if network call failed`() = runBlocking {
        //init
        Mockito.`when`(mockRealEstateAdDataSource.fetchAds()).thenThrow(IllegalStateException())
        Mockito.`when`(mockRealEstateAdLocalDataSource.getAll()).thenReturn(REAL_ESTATE_LISTING_RAW)
        //run
        val first = sut.fetchRealEstateListings().first()

        //verify
        verify(mockRealEstateAdLocalDataSource).getAll()
        Assert.assertEquals(RealEstateListings(REAL_ESTATE_AD, false), first)
    }

    @Test
    fun `should retrieve real estate ad from local then network data source`() = runBlocking {
        //init
        Mockito.`when`(mockRealEstateAdDataSource.fetchAdById(AN_ID)).thenReturn(REAL_ESTATE_RAW)
        Mockito.`when`(mockRealEstateAdLocalDataSource.getById(AN_ID)).thenReturn(REAL_ESTATE_RAW)
        //run
        val results = sut.fetchRealEstateAdById(AN_ID).toList()
        val first = results.first()
        val second = results[1]


        inOrder(mockRealEstateAdLocalDataSource, mockRealEstateAdDataSource) {
            verify(mockRealEstateAdLocalDataSource).getById(AN_ID)
            verify(mockRealEstateAdDataSource).fetchAdById(AN_ID)
        }
        Assert.assertEquals(REAL_ESTATE_AD_DETAILS, first)
        Assert.assertEquals(REAL_ESTATE_AD_DETAILS, second)
    }

    @Test
    fun `should retrieve real estate ad from network data source if local source is empty`() =
        runBlocking {
            //init
            Mockito.`when`(mockRealEstateAdDataSource.fetchAdById(AN_ID))
                .thenReturn(REAL_ESTATE_RAW)
            Mockito.`when`(mockRealEstateAdLocalDataSource.getById(AN_ID)).thenReturn(null)
            //run
            val results = sut.fetchRealEstateAdById(AN_ID).toList()
            val first = results.first()


            inOrder(mockRealEstateAdLocalDataSource, mockRealEstateAdDataSource) {
                verify(mockRealEstateAdLocalDataSource).getById(AN_ID)
                verify(mockRealEstateAdDataSource).fetchAdById(AN_ID)
            }
            Assert.assertEquals(REAL_ESTATE_AD_DETAILS, first)
            Assert.assertEquals(1, results.size)
        }

    @Test(expected = IllegalStateException::class)
    fun `should throw exception if network call fail`() = runBlocking {
        //init
        Mockito.`when`(mockRealEstateAdDataSource.fetchAdById(AN_ID))
            .thenThrow(IllegalStateException())
        Mockito.`when`(mockRealEstateAdLocalDataSource.getById(AN_ID)).thenReturn(null)
        //run
        sut.fetchRealEstateAdById(AN_ID).toList()
        Unit
    }
}

private val REAL_ESTATE_RAW =
    RealEstateAdRaw(
        2,
        7,
        "Deauville",
        600.0f,
        "https://v.seloger.com/s/crop/590x330/visuels/2/a/l/s/2als8bgr8sd2vezcpsj988mse4olspi5rfzpadqok.jpg",
        3500000.0f,
        "GSL STICKINESS",
        "Maison - Villa",
        11
    )

private val REAL_ESTATE_LISTING_RAW = listOf(
    REAL_ESTATE_RAW
)

private val REAL_ESTATE_AD = listOf(
    RealEstateAd(
        2,
        "Deauville",
        600.0f,
        "https://v.seloger.com/s/crop/590x330/visuels/2/a/l/s/2als8bgr8sd2vezcpsj988mse4olspi5rfzpadqok.jpg",
        3500000.0f
    ),
)

private val REAL_ESTATE_AD_DETAILS = RealEstateAdDetails(
    2,
    7,
    "Deauville",
    600.0f,
    "https://v.seloger.com/s/crop/590x330/visuels/2/a/l/s/2als8bgr8sd2vezcpsj988mse4olspi5rfzpadqok.jpg",
    3500000.0f,
    "GSL STICKINESS",
    "Maison - Villa",
    11
)

private val AN_ID = 1
