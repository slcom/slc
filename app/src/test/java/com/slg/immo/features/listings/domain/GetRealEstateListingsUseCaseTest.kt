package com.slg.immo.features.listings.domain

import com.slg.immo.features.listings.domain.model.RealEstateListings
import com.slg.immo.features.listings.domain.repository.RealEstateAdRepository
import com.slg.immo.utils.REAL_ESTATE_LISTING
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.mock


class GetRealEstateListingsUseCaseTest {
    private val mockRealEstateAdRepository: RealEstateAdRepository = mock()

    private val sut = GetRealEstateListingsUseCase(mockRealEstateAdRepository)

    @Test
    fun `should order real estate listings by city`() = runBlocking {
        //init
        Mockito.`when`(mockRealEstateAdRepository.fetchRealEstateListings())
            .thenReturn(flow { emit(RealEstateListings(REAL_ESTATE_LISTING, true)) })

        //run
        val first = sut().first()

        //verify
        Assert.assertEquals(listOf(3, 2, 4, 1), first.items.map { it.id })
    }

}