package com.slg.immo.features.listings.data.remote

import com.slg.immo.features.listings.data.model.RealEstateAdRaw
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

class RealEstateAdDataSourceTest {

    private val mockWebServer = MockWebServer()

    private val client = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.SECONDS)
        .readTimeout(1, TimeUnit.SECONDS)
        .writeTimeout(1, TimeUnit.SECONDS)
        .build()

    private val api: RealEstateAdService = Retrofit.Builder()
        .baseUrl(mockWebServer.url("/"))
        .client(client)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
        .create(RealEstateAdService::class.java)

    private val sut = RealEstateAdDataSource(api)

    @Test
    fun `should fetch ads correctly`() {
        mockWebServer.enqueueResponse("ads.json", 200)

        runBlocking {
            val actual = sut.fetchAds()

            val expected = listOf(
                RealEstateAdRaw(
                    1,
                    4,
                    "Villers-sur-Mer",
                    250.0f,
                    "https://v.seloger.com/s/crop/590x330/visuels/1/7/t/3/17t3fitclms3bzwv8qshbyzh9dw32e9l0p0udr80k.jpg",
                    1500000.0f,
                    "GSL EXPLORE",
                    "Maison - Villa",
                    8
                ),
                RealEstateAdRaw(
                    2,
                    7,
                    "Deauville",
                    600.0f,
                    "https://v.seloger.com/s/crop/590x330/visuels/2/a/l/s/2als8bgr8sd2vezcpsj988mse4olspi5rfzpadqok.jpg",
                    3500000.0f,
                    "GSL STICKINESS",
                    "Maison - Villa",
                    11
                ),
                RealEstateAdRaw(
                    3,
                    city = "Bordeaux",
                    area = 550.0f,
                    price = 3000000.0f,
                    professional = "GSL OWNERS",
                    propertyType = "Maison - Villa",
                    rooms = 7
                ),
                RealEstateAdRaw(
                    4,
                    city = "Nice",
                    area = 250.0f,
                    url = "https://v.seloger.com/s/crop/590x330/visuels/1/9/f/x/19fx7n4og970dhf186925d7lrxv0djttlj5k9dbv8.jpg",
                    price = 5000000.0f,
                    professional = "GSL CONTACTING",
                    propertyType = "Maison - Villa"
                )
            )

            Assert.assertEquals(expected, actual)
        }
    }


    @Test(expected = HttpException::class)
    fun `should throw exception if code is 400`() {
        mockWebServer.enqueueResponse("ads.json", 400)

        runBlocking {
            sut.fetchAds()
        }
    }

    @Test
    fun `should fetch ad by id correctly`() {
        mockWebServer.enqueueResponse("ad_2.json", 200)

        runBlocking {
            val actual = sut.fetchAdById(2)

            val expected = RealEstateAdRaw(
                2,
                7,
                "Deauville",
                600.0f,
                "https://v.seloger.com/s/crop/590x330/visuels/2/a/l/s/2als8bgr8sd2vezcpsj988mse4olspi5rfzpadqok.jpg",
                3500000.0f,
                "GSL STICKINESS",
                "Maison - Villa",
                11
            )

            Assert.assertEquals(expected, actual)
        }
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }
}