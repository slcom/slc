package com.slg.immo.features.listings.ui.details

import com.slg.immo.features.listings.domain.GetRealEstateAdByIdUseCase
import com.slg.immo.features.listings.ui.RealEstateMapper
import com.slg.immo.features.listings.ui.details.model.RealEstateAdDetailsState
import com.slg.immo.utils.CoroutinesTest
import com.slg.immo.utils.REAL_ESTATE_AD_DETAILS
import com.slg.immo.utils.REAL_ESTATE_AD_DETAILS_UI_MODEL
import com.slg.immo.utils.collectStates
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.mock

class RealEstateAdDetailsViewModelTest : CoroutinesTest() {


    private val mockGetRealEstateAdById: GetRealEstateAdByIdUseCase = mock()
    private val mockMapper: RealEstateMapper = mock()

    private val sut =
        RealEstateAdDetailsViewModel(testDispatcher, mockGetRealEstateAdById, mockMapper)


    @Test
    fun `should fetch real estate ad details`() = runBlocking {
        //init
        Mockito.`when`(mockMapper.toUiModel(REAL_ESTATE_AD_DETAILS))
            .thenReturn(REAL_ESTATE_AD_DETAILS_UI_MODEL)
        Mockito.`when`(mockGetRealEstateAdById(2))
            .thenReturn(flow { emit(REAL_ESTATE_AD_DETAILS) })
        val states = sut.collectStates()

        //run
        sut.start(2)

        //verify
        Assert.assertEquals(
            listOf(
                RealEstateAdDetailsState.Loading,
                RealEstateAdDetailsState.Result(REAL_ESTATE_AD_DETAILS_UI_MODEL)
            ),
            states
        )
    }
}