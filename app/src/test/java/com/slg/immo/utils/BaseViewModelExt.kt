package com.slg.immo.utils

import androidx.lifecycle.viewModelScope
import com.slg.immo.common.mvi.BaseViewModel
import com.slg.immo.common.mvi.Effect
import com.slg.immo.common.mvi.State
import kotlinx.coroutines.launch

fun <T : State> BaseViewModel<T>.collectStates(): List<State> {
    return mutableListOf<State>()
        .also { list ->
            viewModelScope.launch {
                states.collect { list.add(it) }
            }
        }
}


fun <T : State> BaseViewModel<T>.collectEffect(): List<Effect> {
    return mutableListOf<Effect>()
        .also { list ->
            viewModelScope.launch {
                effects.collect { list.add(it) }
            }
        }
}