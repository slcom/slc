package com.slg.immo.utils

import org.junit.Rule

open class CoroutinesTest {

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    val testDispatcher = coroutinesTestRule.testDispatcher
}