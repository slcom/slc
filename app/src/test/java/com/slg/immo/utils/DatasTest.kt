package com.slg.immo.utils

import com.slg.immo.features.listings.domain.model.RealEstateAd
import com.slg.immo.features.listings.domain.model.RealEstateAdDetails
import com.slg.immo.features.listings.ui.details.model.RealEstateAdDetailsUiModel
import com.slg.immo.features.listings.ui.list.model.RealEstateUiModel


val REAL_ESTATE_LISTING = listOf(
    RealEstateAd(
        1,
        "Villers-sur-Mer",
        250.0f,
        "https://v.seloger.com/s/crop/590x330/visuels/1/7/t/3/17t3fitclms3bzwv8qshbyzh9dw32e9l0p0udr80k.jpg",
        1500000.0f
    ),
    RealEstateAd(
        2,
        "Deauville",
        600.0f,
        "https://v.seloger.com/s/crop/590x330/visuels/2/a/l/s/2als8bgr8sd2vezcpsj988mse4olspi5rfzpadqok.jpg",
        3500000.0f
    ),
    RealEstateAd(
        3,
        city = "Bordeaux",
        area = 550.0f,
        price = 3000000.0f
    ),
    RealEstateAd(
        4,
        city = "Nice",
        area = 250.0f,
        url = "https://v.seloger.com/s/crop/590x330/visuels/1/9/f/x/19fx7n4og970dhf186925d7lrxv0djttlj5k9dbv8.jpg",
        price = 5000000.0f
    )
)


val REAL_ESTATE_LISTING_UI_MODEL = listOf(
    RealEstateUiModel.CityUiModel("Villers-sur-Mer"),
    RealEstateUiModel.RealEstateAdUiModel(
        1,
        "Villers-sur-Mer",
        "250.0",
        "https://v.seloger.com/s/crop/590x330/visuels/1/7/t/3/17t3fitclms3bzwv8qshbyzh9dw32e9l0p0udr80k.jpg",
        "1500000.0"
    ),
    RealEstateUiModel.CityUiModel("Deauville"),
    RealEstateUiModel.RealEstateAdUiModel(
        2,
        "Deauville",
        "600.0",
        "https://v.seloger.com/s/crop/590x330/visuels/2/a/l/s/2als8bgr8sd2vezcpsj988mse4olspi5rfzpadqok.jpg",
        "3500000.0"
    ),
    RealEstateUiModel.CityUiModel("Bordeaux"),
    RealEstateUiModel.RealEstateAdUiModel(
        3,
        city = "Bordeaux",
        area = "550.0",
        price = "3000000.0"
    ),
    RealEstateUiModel.CityUiModel("Nice"),
    RealEstateUiModel.RealEstateAdUiModel(
        4,
        city = "Nice",
        area = "250.0",
        url = "https://v.seloger.com/s/crop/590x330/visuels/1/9/f/x/19fx7n4og970dhf186925d7lrxv0djttlj5k9dbv8.jpg",
        price = "5000000.0"
    )
)

val REAL_ESTATE_AD_DETAILS =
    RealEstateAdDetails(
        2,
        7,
        "Deauville",
        600.0f,
        "https://v.seloger.com/s/crop/590x330/visuels/2/a/l/s/2als8bgr8sd2vezcpsj988mse4olspi5rfzpadqok.jpg",
        3500000.0f,
        "GSL STICKINESS",
        "Maison - Villa",
        11
    )

val REAL_ESTATE_AD_DETAILS_UI_MODEL = RealEstateAdDetailsUiModel(
    2,
    "Deauville",
    "600 m² . 11 pièces . 7 chambres",
    "https://v.seloger.com/s/crop/590x330/visuels/2/a/l/s/2als8bgr8sd2vezcpsj988mse4olspi5rfzpadqok.jpg",
    "3 500 000 €",
    "GSL STICKINESS",
    "Maison - Villa"
)