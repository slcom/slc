package com.slg.immo.di

import android.content.Context
import androidx.room.Room
import com.slg.immo.common.image.ImageLoader
import com.slg.immo.features.listings.data.local.AppDatabase
import com.slg.immo.features.listings.data.local.RealEstateAdDao
import com.slg.immo.features.listings.data.remote.RealEstateAdService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object CommonSingletonProviderModule {

    @Provides
    fun provideRealEstateAdService(): RealEstateAdService {

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(RealEstateAdService::class.java)
    }

    @Provides
    @Singleton
    fun provideCoroutineDispatcher(): CoroutineDispatcher {
        return Dispatchers.IO
    }

    @Provides
    @Singleton
    fun provideImageLoader(@ApplicationContext context: Context): ImageLoader {
        return ImageLoader(context)
    }

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "immo.db"
        ).build()
    }

    @Provides
    fun provideRealEstateAdDao(appDatabase: AppDatabase): RealEstateAdDao {
        return appDatabase.realEstateAdDao()
    }
}

private const val BASE_URL = "https://gsl-apps-technical-test.dignp.com/"