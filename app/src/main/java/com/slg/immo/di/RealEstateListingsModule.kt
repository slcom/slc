package com.slg.immo.di

import com.slg.immo.features.listings.data.ImmoRealEstateAdRepository
import com.slg.immo.features.listings.data.local.RealEstateAdLocalDataSource
import com.slg.immo.features.listings.data.local.RealEstateAdRoomDataSource
import com.slg.immo.features.listings.domain.repository.RealEstateAdRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent


@Module
@InstallIn(ViewModelComponent::class)
abstract class DataSourceModule {

    @Binds
    abstract fun bindRealEstateAdLocalDataSource(
        realEstateAdRoomDataSource: RealEstateAdRoomDataSource
    ): RealEstateAdLocalDataSource
}


@Module
@InstallIn(ViewModelComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindRealEstateAdRepository(
        immoRealEstateAdRepository: ImmoRealEstateAdRepository
    ): RealEstateAdRepository
}
