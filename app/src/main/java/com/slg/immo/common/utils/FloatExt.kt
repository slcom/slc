package com.slg.immo.common.utils

import java.text.NumberFormat
import java.util.*

fun Float.formatPrice(): String {
    val format: NumberFormat = NumberFormat.getCurrencyInstance()
    format.maximumFractionDigits = 0
    format.currency = Currency.getInstance("EUR")

    return format.format(this.toInt())
}

fun Float.formatArea(): String = "${toInt()} m²"
