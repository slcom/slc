package com.slg.immo.common.mvi

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class BaseViewModel<T : State>(
    private val dispatcher: CoroutineDispatcher,
    initialState: T
) : ViewModel() {

    private val innerStates: MutableStateFlow<T> = MutableStateFlow(initialState)
    private val innerEffects: MutableSharedFlow<Effect> = MutableSharedFlow()

    val states: Flow<T> by ::innerStates
    val effects: Flow<Effect> by ::innerEffects


    suspend fun publishState(state: T) {
        innerStates.emit(state)
    }

    suspend fun publishEvent(effect: Effect) {
        innerEffects.emit(effect)
    }

    fun observeStates(lifecycleOwner: LifecycleOwner, block: (T) -> Unit) {
        states.asLiveData(viewModelScope.coroutineContext).observe(lifecycleOwner, block)
    }

    fun observeEffects(lifecycleOwner: LifecycleOwner, block: (Effect) -> Unit) {
        effects.asLiveData(viewModelScope.coroutineContext).observe(lifecycleOwner, block)
    }

    protected fun <F> Flow<F>.handleAsync(
        onSuccess: suspend (F) -> Unit,
        onError: suspend (e: Throwable) -> Unit
    ) =
        viewModelScope.launch {
            withContext(dispatcher) {
                this@handleAsync
                    .catch { exception -> onError(exception) }
                    .collect { onSuccess(it) }
            }
        }
}

interface Event
interface State : Event
interface Effect : Event


