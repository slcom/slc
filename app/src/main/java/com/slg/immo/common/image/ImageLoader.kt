package com.slg.immo.common.image

import android.content.Context
import android.widget.ImageView
import androidx.annotation.ColorRes
import com.bumptech.glide.Glide
import com.slg.immo.R
import javax.inject.Inject

class ImageLoader @Inject constructor(private val context: Context) {

    fun loadUrl(imageView: ImageView, url: String?, @ColorRes placeholder: Int = R.color.grey) {
        Glide.with(context).load(url).placeholder(placeholder).into(imageView)
    }
}