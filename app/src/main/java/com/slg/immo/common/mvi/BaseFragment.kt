package com.slg.immo.common.mvi

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment

abstract class BaseFragment<T : State> : Fragment() {

    protected abstract val viewModel: BaseViewModel<T>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.observeStates(viewLifecycleOwner, ::handleState)
        viewModel.observeEffects(viewLifecycleOwner, ::handleEffect)

    }

    protected open fun handleState(state: T): Unit = Unit

    protected open fun handleEffect(effect: Effect): Unit = Unit
}