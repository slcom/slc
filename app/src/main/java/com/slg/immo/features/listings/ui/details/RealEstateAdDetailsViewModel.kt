package com.slg.immo.features.listings.ui.details

import com.slg.immo.common.mvi.BaseViewModel
import com.slg.immo.features.listings.domain.GetRealEstateAdByIdUseCase
import com.slg.immo.features.listings.ui.RealEstateMapper
import com.slg.immo.features.listings.ui.details.model.RealEstateAdDetailsState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class RealEstateAdDetailsViewModel @Inject constructor(
    dispatcher: CoroutineDispatcher,
    private val getRealEstateAdById: GetRealEstateAdByIdUseCase,
    private val realEstateMapper: RealEstateMapper
) : BaseViewModel<RealEstateAdDetailsState>(dispatcher, RealEstateAdDetailsState.Loading) {

    fun start(id: Int?) {
        if (id != null) {
            getRealEstateAdById(id).handleAsync({ realEstateAd ->
                val realEstateAdDetails = realEstateMapper.toUiModel(realEstateAd)

                publishState(RealEstateAdDetailsState.Result(realEstateAdDetails))
            }, {
                Timber.d(it, "Erreur lors de la récupération de l'annonce")
            })
        } else {
            Timber.d("Impossible de récupéréune annonce sans id")
        }
    }
}
