package com.slg.immo.features.listings.ui.list

import com.slg.immo.common.mvi.BaseViewModel
import com.slg.immo.features.listings.domain.GetRealEstateListingsUseCase
import com.slg.immo.features.listings.ui.RealEstateMapper
import com.slg.immo.features.listings.ui.list.model.OldDataWarning
import com.slg.immo.features.listings.ui.list.model.RealEstateListingState
import com.slg.immo.features.listings.ui.list.model.RealEstateListingState.Loading
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class RealEstateListingsViewModel @Inject constructor(
    dispatcher: CoroutineDispatcher,
    private val getRealEstateListings: GetRealEstateListingsUseCase,
    private val realEstateMapper: RealEstateMapper
) : BaseViewModel<RealEstateListingState>(dispatcher, Loading) {

    fun start() {
        getRealEstateListings().handleAsync({ realEstateListings ->
            val listingWithHeader = realEstateMapper.toUiModel(realEstateListings.items)
            val nextState = if (listingWithHeader.isEmpty()) {
                RealEstateListingState.EmptyResult
            } else {
                RealEstateListingState.Result(listingWithHeader)
            }
            sendWarningIfNecessary(realEstateListings.isFreshData)
            publishState(nextState)
        }, {
            publishState(RealEstateListingState.EmptyResult)
            Timber.d(it, "Erreur lors de la récupération des annonces")
        })
    }

    private suspend fun sendWarningIfNecessary(isFreshData: Boolean) {
        if (!isFreshData) {
            publishEvent(OldDataWarning)
        }
    }
}