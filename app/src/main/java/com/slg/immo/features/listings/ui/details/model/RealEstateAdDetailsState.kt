package com.slg.immo.features.listings.ui.details.model

import com.slg.immo.common.mvi.State

sealed interface RealEstateAdDetailsState : State {

    object Loading : RealEstateAdDetailsState
    data class Result(val item: RealEstateAdDetailsUiModel) : RealEstateAdDetailsState
}
