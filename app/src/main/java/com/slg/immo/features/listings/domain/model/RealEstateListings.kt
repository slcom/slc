package com.slg.immo.features.listings.domain.model

data class RealEstateListings(
    val items: List<RealEstateAd>,
    val isFreshData: Boolean
)