package com.slg.immo.features.listings.data.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.slg.immo.features.listings.data.model.RealEstateAdRaw

@Dao
interface RealEstateAdDao {

    @Query("SELECT * FROM realEstateAds")
    fun getAll(): List<RealEstateAdRaw>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(ads: List<RealEstateAdRaw>)

    @Delete
    fun deleteAll(ads: List<RealEstateAdRaw>)

    @Query("SELECT * FROM realEstateAds WHERE id=:id ")
    abstract fun getById(id: String): RealEstateAdRaw
}