package com.slg.immo.features.listings.ui.list.model

sealed class RealEstateUiModel {

    data class CityUiModel(val city: String) : RealEstateUiModel()

    data class RealEstateAdUiModel(
        val id: Int,
        val city: String,
        val area: String,
        val url: String? = null,
        val price: String? = null
    ) : RealEstateUiModel()
}

