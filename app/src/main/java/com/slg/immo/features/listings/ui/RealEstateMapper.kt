package com.slg.immo.features.listings.ui

import android.content.Context
import androidx.annotation.PluralsRes
import com.slg.immo.R
import com.slg.immo.common.utils.formatArea
import com.slg.immo.common.utils.formatPrice
import com.slg.immo.features.listings.domain.model.RealEstateAd
import com.slg.immo.features.listings.domain.model.RealEstateAdDetails
import com.slg.immo.features.listings.ui.details.model.RealEstateAdDetailsUiModel
import com.slg.immo.features.listings.ui.list.model.RealEstateUiModel
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class RealEstateMapper @Inject constructor(@ApplicationContext private val context: Context) {


    fun toUiModel(realEstateListings: List<RealEstateAd>): List<RealEstateUiModel> =
        with(realEstateListings) {
            groupBy { it.city }.entries.flatMap {
                mutableListOf<RealEstateUiModel>(RealEstateUiModel.CityUiModel(it.key)).apply {
                    addAll(it.value.map { realEstateAd -> toUiModel(realEstateAd) })
                }
            }
        }

    private fun toUiModel(realEstateAd: RealEstateAd): RealEstateUiModel.RealEstateAdUiModel =
        with(realEstateAd) {
            RealEstateUiModel.RealEstateAdUiModel(
                id,
                city,
                area.formatArea(),
                url,
                price.formatPrice()
            )
        }


    fun toUiModel(realEstateAdDetails: RealEstateAdDetails): RealEstateAdDetailsUiModel =
        with(realEstateAdDetails) {

            val characteristics = StringBuilder().apply {
                append(area.formatArea())
                rooms?.let {
                    append(
                        addPrefixAndSuffix(R.plurals.room_label, it)
                    )
                }
                bedrooms?.let {
                    append(
                        addPrefixAndSuffix(R.plurals.bedroom_label, it)
                    )
                }
            }.toString()

            RealEstateAdDetailsUiModel(
                id,
                city,
                characteristics,
                url,
                price.formatPrice(),
                professional,
                propertyType,
            )
        }

    private fun addPrefixAndSuffix(@PluralsRes resource: Int, value: Int) = " . ${
        context.resources.getQuantityString(
            resource,
            value,
            value
        )
    }"
}