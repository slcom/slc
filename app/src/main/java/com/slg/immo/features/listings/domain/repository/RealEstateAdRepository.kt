package com.slg.immo.features.listings.domain.repository

import com.slg.immo.features.listings.domain.model.RealEstateAdDetails
import com.slg.immo.features.listings.domain.model.RealEstateListings
import kotlinx.coroutines.flow.Flow

interface RealEstateAdRepository {
    fun fetchRealEstateListings(): Flow<RealEstateListings>
    fun fetchRealEstateAdById(id: Int): Flow<RealEstateAdDetails>
}