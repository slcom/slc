package com.slg.immo.features.listings.domain.model

data class RealEstateAd(
    val id: Int,
    val city: String,
    val area: Float,
    val url: String? = null,
    val price: Float
)