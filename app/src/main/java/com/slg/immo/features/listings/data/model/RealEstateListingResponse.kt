package com.slg.immo.features.listings.data.model

import androidx.room.Entity

@Entity(tableName = "realEstateAds")
data class RealEstateListingResponse(

    val items: List<RealEstateAdRaw>
)
