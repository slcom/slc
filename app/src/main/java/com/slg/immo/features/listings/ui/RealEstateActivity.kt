package com.slg.immo.features.listings.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.slg.immo.databinding.ActivityRealEstateListingsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RealEstateActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRealEstateListingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRealEstateListingsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return true
    }
}