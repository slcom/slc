package com.slg.immo.features.listings.domain

import com.slg.immo.features.listings.domain.model.RealEstateAdDetails
import com.slg.immo.features.listings.domain.repository.RealEstateAdRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetRealEstateAdByIdUseCase @Inject constructor(private val repository: RealEstateAdRepository) {

    operator fun invoke(id: Int): Flow<RealEstateAdDetails> = repository.fetchRealEstateAdById(id)

}