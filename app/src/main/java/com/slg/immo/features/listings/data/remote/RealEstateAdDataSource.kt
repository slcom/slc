package com.slg.immo.features.listings.data.remote

import com.slg.immo.features.listings.data.model.RealEstateAdRaw
import javax.inject.Inject

class RealEstateAdDataSource @Inject constructor(private val realEstateAdService: RealEstateAdService) {

    suspend fun fetchAds(): List<RealEstateAdRaw> = realEstateAdService.fetchAds().items

    suspend fun fetchAdById(id: Int): RealEstateAdRaw = realEstateAdService.fetchAdById(id)

}