package com.slg.immo.features.listings.data.local

import com.slg.immo.features.listings.data.model.RealEstateAdRaw


interface RealEstateAdLocalDataSource {

    fun getAll(): List<RealEstateAdRaw>
    fun getById(id: Int): RealEstateAdRaw?
    fun insertAll(ads: List<RealEstateAdRaw>)
}