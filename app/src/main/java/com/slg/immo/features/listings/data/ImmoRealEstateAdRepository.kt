package com.slg.immo.features.listings.data

import com.slg.immo.features.listings.data.local.RealEstateAdLocalDataSource
import com.slg.immo.features.listings.data.model.toDetailsDomain
import com.slg.immo.features.listings.data.model.toDomain
import com.slg.immo.features.listings.data.remote.RealEstateAdDataSource
import com.slg.immo.features.listings.domain.model.RealEstateAdDetails
import com.slg.immo.features.listings.domain.model.RealEstateListings
import com.slg.immo.features.listings.domain.repository.RealEstateAdRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import timber.log.Timber
import javax.inject.Inject

class ImmoRealEstateAdRepository @Inject constructor(
    private val networkDataSource: RealEstateAdDataSource,
    private val localDataSource: RealEstateAdLocalDataSource,
) : RealEstateAdRepository {

    override fun fetchRealEstateListings(): Flow<RealEstateListings> =
        flow {
            val ads = networkDataSource.fetchAds()
            localDataSource.insertAll(ads)
            emit(ads.toDomain(true))
        }.catch {
            Timber.e(it, "Impossible de récupérer les données depuis le web")
            emit(localDataSource.getAll().toDomain(false))
        }


    override fun fetchRealEstateAdById(id: Int): Flow<RealEstateAdDetails> =
        flow {
            localDataSource.getById(id)?.let { emit(it.toDetailsDomain()) }
            emit(networkDataSource.fetchAdById(id).toDetailsDomain())
        }.catch {
            Timber.e(it, "Impossible de récupérer les détails de l'annonce d'id $id")
            throw it
        }
}