package com.slg.immo.features.listings.domain

import com.slg.immo.features.listings.domain.model.RealEstateListings
import com.slg.immo.features.listings.domain.repository.RealEstateAdRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GetRealEstateListingsUseCase @Inject constructor(private val repository: RealEstateAdRepository) {

    operator fun invoke(): Flow<RealEstateListings> = repository.fetchRealEstateListings()
        .map { realEstateListings ->
            realEstateListings.copy(
                items = realEstateListings.items.sortedWith(
                    compareBy { it.city }
                )
            )
        }
}