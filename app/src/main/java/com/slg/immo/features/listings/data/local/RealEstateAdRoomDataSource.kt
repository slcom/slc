package com.slg.immo.features.listings.data.local

import com.slg.immo.features.listings.data.model.RealEstateAdRaw
import javax.inject.Inject

class RealEstateAdRoomDataSource @Inject constructor(private val realEstateAdDao: RealEstateAdDao) :
    RealEstateAdLocalDataSource {

    override fun getAll(): List<RealEstateAdRaw> = realEstateAdDao.getAll()

    override fun getById(id: Int): RealEstateAdRaw = realEstateAdDao.getById(id.toString())

    override fun insertAll(ads: List<RealEstateAdRaw>) = realEstateAdDao.insertAll(ads)
}