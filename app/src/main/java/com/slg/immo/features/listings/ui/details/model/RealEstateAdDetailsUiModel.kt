package com.slg.immo.features.listings.ui.details.model

data class RealEstateAdDetailsUiModel(
    val id: Int,
    val city: String,
    val characteristics: String,
    val url: String? = null,
    val price: String,
    val professional: String,
    val propertyType: String
)
