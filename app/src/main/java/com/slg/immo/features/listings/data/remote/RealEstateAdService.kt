package com.slg.immo.features.listings.data.remote

import com.slg.immo.features.listings.data.model.RealEstateAdRaw
import com.slg.immo.features.listings.data.model.RealEstateListingResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface RealEstateAdService {
    @GET("listings.json")
    suspend fun fetchAds(): RealEstateListingResponse

    @GET("listings/{listingId}.json")
    suspend fun fetchAdById(@Path("listingId") id: Int): RealEstateAdRaw
}
