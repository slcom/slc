package com.slg.immo.features.listings.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.slg.immo.features.listings.data.model.RealEstateAdRaw

@Database(entities = [RealEstateAdRaw::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun realEstateAdDao(): RealEstateAdDao
}