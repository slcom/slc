package com.slg.immo.features.listings.ui.list.model

import com.slg.immo.common.mvi.Effect
import com.slg.immo.common.mvi.State

sealed interface RealEstateListingState : State {

    object Loading : RealEstateListingState
    object EmptyResult : RealEstateListingState
    data class Result(val items: List<RealEstateUiModel>) : RealEstateListingState
}


object OldDataWarning : Effect
