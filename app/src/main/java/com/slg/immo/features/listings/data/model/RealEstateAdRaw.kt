package com.slg.immo.features.listings.data.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.slg.immo.features.listings.domain.model.RealEstateAd
import com.slg.immo.features.listings.domain.model.RealEstateAdDetails
import com.slg.immo.features.listings.domain.model.RealEstateListings

@Entity(tableName = "realEstateAds")
data class RealEstateAdRaw(

    @NonNull
    @PrimaryKey
    val id: Int,
    val bedrooms: Int? = null,
    val city: String,
    val area: Float,
    val url: String? = null,
    val price: Float,
    val professional: String,
    val propertyType: String,
    val rooms: Int? = null
)


fun List<RealEstateAdRaw>.toDomain(isFreshData: Boolean): RealEstateListings =
    RealEstateListings(
        items = map { it.toDomain() },
        isFreshData = isFreshData
    )

private fun RealEstateAdRaw.toDomain(): RealEstateAd =
    RealEstateAd(id, city, area, url, price)


fun RealEstateAdRaw.toDetailsDomain(): RealEstateAdDetails =
    RealEstateAdDetails(id, bedrooms, city, area, url, price, professional, propertyType, rooms)
