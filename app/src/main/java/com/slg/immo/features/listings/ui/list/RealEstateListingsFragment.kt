package com.slg.immo.features.listings.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.slg.immo.R
import com.slg.immo.common.image.ImageLoader
import com.slg.immo.common.mvi.BaseFragment
import com.slg.immo.common.mvi.Effect
import com.slg.immo.databinding.FragmentRealEstateListingsBinding
import com.slg.immo.features.listings.ui.list.model.OldDataWarning
import com.slg.immo.features.listings.ui.list.model.RealEstateListingState
import com.slg.immo.features.listings.ui.list.model.RealEstateListingState.*
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class RealEstateListingsFragment : BaseFragment<RealEstateListingState>() {

    override val viewModel: RealEstateListingsViewModel by viewModels()
    private var binding: FragmentRealEstateListingsBinding? = null


    @Inject
    lateinit var imageLoader: ImageLoader

    var realEstateListingAdapter: RealEstateListingAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.start()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        FragmentRealEstateListingsBinding.inflate(inflater, container, false)
            .apply {
                binding = this
                initUi()
            }.root


    private fun initUi() {
        realEstateListingAdapter = RealEstateListingAdapter(imageLoader, ::onItemClicked)
        binding?.realEstateListings?.apply {
            setHasFixedSize(true)
            itemAnimator = null
            adapter = realEstateListingAdapter
        }
    }

    private fun onItemClicked(id: Int) {
        val action =
            RealEstateListingsFragmentDirections.actionListingToDetails(id)
        findNavController().navigate(action)
    }

    override fun handleEffect(effect: Effect): Unit =
        when (effect) {
            OldDataWarning -> {
                Toast.makeText(
                    activity,
                    getString(R.string.fetch_data_error_message),
                    Toast.LENGTH_LONG
                )
                    .show()
            }
            else -> Unit
        }


    override fun handleState(state: RealEstateListingState) {
        binding?.apply {
            when (state) {
                is Result -> {
                    realEstateListings.isVisible = true
                    realEstateListingAdapter?.replaceAll(state.items)
                    loader.hide()
                }
                is Loading -> {
                    loader.show()
                }
                is EmptyResult -> {
                    loader.hide()
                    emptyWarning.isVisible = true
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance() = RealEstateListingsFragment()
    }
}

