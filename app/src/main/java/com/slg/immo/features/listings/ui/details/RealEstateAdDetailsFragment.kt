package com.slg.immo.features.listings.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.slg.immo.common.image.ImageLoader
import com.slg.immo.common.mvi.BaseFragment
import com.slg.immo.databinding.FragmentRealEstateAdDetailsBinding
import com.slg.immo.features.listings.ui.details.model.RealEstateAdDetailsState
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


private const val ARG_ID = "id"

@AndroidEntryPoint
class RealEstateAdDetailsFragment : BaseFragment<RealEstateAdDetailsState>() {

    override val viewModel: RealEstateAdDetailsViewModel by viewModels()
    private var binding: FragmentRealEstateAdDetailsBinding? = null

    @Inject
    lateinit var imageLoader: ImageLoader

    private val id: Int?
        get() = arguments?.getInt(ARG_ID)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.start(id)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        FragmentRealEstateAdDetailsBinding.inflate(inflater, container, false)
            .apply {
                binding = this
            }.root


    override fun handleState(state: RealEstateAdDetailsState) {
        binding?.apply {
            when (state) {
                is RealEstateAdDetailsState.Result -> {
                    val ad = state.item
                    price.text = ad.price
                    characteristics.text = ad.characteristics
                    professional.text = ad.professional
                    city.text = ad.city
                    type.text = ad.propertyType
                    imageLoader.loadUrl(picture, ad.url)
                    loader.hide()
                }
                is RealEstateAdDetailsState.Loading -> {
                    loader.show()
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance(id: Int) =
            RealEstateAdDetailsFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_ID, id)
                }
            }
    }
}