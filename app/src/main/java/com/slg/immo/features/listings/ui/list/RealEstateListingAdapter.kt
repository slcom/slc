package com.slg.immo.features.listings.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.slg.immo.R
import com.slg.immo.common.image.ImageLoader
import com.slg.immo.databinding.CityHeaderItemBinding
import com.slg.immo.databinding.RealEstateAdItemBinding
import com.slg.immo.features.listings.ui.list.model.RealEstateUiModel

class RealEstateListingAdapter constructor(
    private val imageLoader: ImageLoader,
    private val onItemClicked: (Int) -> Unit
) :
    RecyclerView.Adapter<RealEstateListingAdapter.ViewHolder>() {

    private var dataSet: List<RealEstateUiModel> = emptyList()

    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    class HeaderViewHolder(view: View) : ViewHolder(view) {

        fun onBindViewHolder(cityUiModel: RealEstateUiModel.CityUiModel) {
            CityHeaderItemBinding.bind(itemView).apply {
                header.text = cityUiModel.city
            }
        }
    }

    class RealEstateAdViewHolder(view: View, val onItemClicked: (Int) -> Unit) : ViewHolder(view) {

        fun onBindViewHolder(
            imageLoader: ImageLoader,
            realEstateAd: RealEstateUiModel.RealEstateAdUiModel
        ) {
            RealEstateAdItemBinding.bind(itemView).apply {
                itemView.setOnClickListener { onItemClicked(realEstateAd.id) }
                area.text = realEstateAd.area
                price.text = realEstateAd.price
                price.isVisible = realEstateAd.price != null
                imageLoader.loadUrl(picture, realEstateAd.url)
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return when (ItemType.values()[viewType]) {
            ItemType.HEADER -> {
                val view = getView(viewGroup, R.layout.city_header_item)
                HeaderViewHolder(view)
            }
            ItemType.AD -> {
                val view = getView(viewGroup, R.layout.real_estate_ad_item)
                RealEstateAdViewHolder(view, onItemClicked)
            }
        }
    }

    private fun getView(viewGroup: ViewGroup, @LayoutRes resource: Int): View =
        LayoutInflater.from(viewGroup.context)
            .inflate(resource, viewGroup, false)


    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        return when (val item = dataSet[position]) {
            is RealEstateUiModel.RealEstateAdUiModel -> {
                (viewHolder as RealEstateAdViewHolder).onBindViewHolder(imageLoader, item)
            }
            is RealEstateUiModel.CityUiModel -> {
                (viewHolder as HeaderViewHolder).onBindViewHolder(item)
            }
        }
    }

    override fun getItemCount() = dataSet.size

    fun replaceAll(realEstateListing: List<RealEstateUiModel>) {
        dataSet = realEstateListing
        notifyDataSetChanged()
    }


    override fun getItemViewType(position: Int): Int {
        return when (dataSet[position]) {
            is RealEstateUiModel.RealEstateAdUiModel -> ItemType.AD.ordinal
            is RealEstateUiModel.CityUiModel -> ItemType.HEADER.ordinal
        }
    }

}


enum class ItemType {
    HEADER, AD
}