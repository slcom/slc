package com.slg.immo

import android.app.Application
import com.slg.immo.common.LoggingTree
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class ImmoApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(LoggingTree())
    }
}