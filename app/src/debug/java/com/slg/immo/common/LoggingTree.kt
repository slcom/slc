package com.slg.immo.common

import timber.log.Timber

class LoggingTree : Timber.DebugTree() {

    override fun createStackElementTag(element: StackTraceElement): String =
        "(${element.fileName}:${element.lineNumber})"
}
