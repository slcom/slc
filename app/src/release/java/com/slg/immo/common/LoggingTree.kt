package com.slg.immo.common

import android.annotation.SuppressLint
import android.util.Log
import timber.log.Timber

class LoggingTree : Timber.Tree() {

    override fun isLoggable(tag: String?, priority: Int) = when (priority) {
        Log.DEBUG, Log.INFO, Log.VERBOSE -> false
        else -> true
    }

    @SuppressLint("LogNotTimber")
    override fun log(priority: Int, tag: String?, message: String, exception: Throwable?) {
        if (isLoggable(tag, priority)) {
            if (priority == Log.ASSERT) {
                Log.wtf(tag, message)
            } else {
                Log.println(priority, tag, message)
            }
        }
    }
}