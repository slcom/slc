

Immo
=======

# Choix d'architecture et d'implémentation:

 Séparation de l'app en trois couches présentation/métier/data pilotées par du DDD.
- Sur la partie présentation on trouve un MVI avec la vue qui interagit avec le model par simple appels à des méthodes, non par envoie d’évènements.
- Sur la partie domain on s'appuie sur des classes UseCase. Le but étant d'y mettre le plus possible les besoins métier (tri etc..).
- La partie data repose sur un pattern repository avec l'appuie de data sources (network, local storage...)
Chaque couche échange à l'aide de DTO définis dans domaine.
- Les logs sont desactivé en release et proguard est configuré.

# Choix des librairie
- Hilt pour l'injection de dépendance. Mon choix aurait pu se porter sur Koin mais j'ai préféré une injection à la compilation pour des raisons de performances, ainsi que la capacité de voir les erreur à la compilation et non au runtime.
- Retrofit pour les appels réseaux.
- Moshi afin de parser le Json. Plus performant que Gson par exemple (et plus adapté pour le Kotlin), surtout pour de gros volumes de données.
- Room pour la persistance. Librairie de Jetpack, elle permet d'implémenter rapidement un stockage local maintenable. De plus couplé avec Flow elle donne davantage de flexibilité dans les choix fonctionnels.
- La gestion de l'asynchronisme repose sur de la programmation réactive avec Kotlin Flow + Coroutine.
- ViewModel pour conserver les données sans dépendre du cycle de vie des activités/fragments
- LiveData afin de fournir les données à l'activité lorsque celle-ci est prète à les recevoir.
- Glide afin d'afficher les photos des titres. J'ai pu constater qu'avec une autre librairie comme Picasso, le scroll de ma liste était moins fluide lorsqu'on affiche beaucoup d'annonces comparé à Glide.


# Pour aller plus loin (avec plus de temps)

- Proposer à l'utilisateur une action pour revenir en haut de la liste et implémenter un pull to refresh
- Mettre en place une gestion des exceptions (avec l'implémentation d'un intercepteur)
- Définir une icône, un splashscreen et un thème.
- Utilisation de Kotlin pour les scripts gradle (ktx)
